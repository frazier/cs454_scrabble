
package gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.*;

import board.Board;
import board.Tile;
import player.Player;
import dictionary.Dawg;

import java.awt.image.ImageObserver;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author sean
 */
public class ScrabbleWindow extends JFrame implements ActionListener{

    public static final int TILE_SIZE = 10;

    private Board board;
    private JPanel scorePanel = new JPanel();
    private JLabel score1Label = new JLabel();
    private JLabel score2Label = new JLabel();
    private JPanel gridPanel = new JPanel();
    private JPanel buttonPanel = new JPanel();
    private JButton[][] tiles;
    private ArrayList<JButton> toDisable;
    private ArrayList <Point> played;

    Callback scrabbleGame;

    //Implement player
    private Player player1, player2, currentPlayer;
    private ArrayList<Character> playerTiles;

    JButton newGameButton;
    JButton endTurnButton;
    GridAction gridAction;

    public ScrabbleWindow(Callback scrabbleGame, Board board, Player player1, Player player2, Player activePlayer) {
        setLayout(new BorderLayout());
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension screenSize = tk.getScreenSize();
        setLocation(screenSize.width / 4, screenSize.height / 4);
        this.toDisable=new ArrayList<JButton>();
        this.board = board;
        this.player1 = player1;
        this.currentPlayer = activePlayer;
        this.player2 = player2;
        this.scrabbleGame = scrabbleGame;
        this.playerTiles = new ArrayList(this.currentPlayer.getTiles());
        tiles = new JButton[board.getSize()][board.getSize()];
        played = new ArrayList<>();
        initGUI();
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
    }

    public interface Callback
    {
        public boolean validMove(String checkWord);

        public Player switchPlayer();
    }

    public void initGUI(){
        initButtons();
        initGrid();
        initScorePanel();
        add(buttonPanel, BorderLayout.NORTH);
        add(gridPanel);
        add(scorePanel, BorderLayout.SOUTH);
        updateBoard();
    }

    public void initButtons(){
        buttonPanel.setLayout(new GridLayout(1,2));
        newGameButton = new JButton("New Game");
        endTurnButton = new JButton("End Turn");
        newGameButton.addActionListener(this);
        endTurnButton.addActionListener(this);
        buttonPanel.add(newGameButton);
        buttonPanel.add(endTurnButton);
    }

    private void initGrid() {
        gridAction = new GridAction(this, currentPlayer);
        gridPanel.setLayout(new GridLayout(board.getSize(), board.getSize()));
        for (int row = 0; row < board.getSize(); row++) {
            for (int column = 0; column < board.getSize(); column++) {
                Tile tile = board.getTile(row, column);


                JButton button = new JButton();
                button.addActionListener(this);
                button.setText("" + tile.getContent());
                button.setSize(50, 50);
                //button.setAction(gridAction);

                tiles[row][column] = button;
                gridPanel.add(button);
            }
        }
    }

    public void initScorePanel() {
        scorePanel.setLayout(new GridLayout(1, 2));
        //score1Label.setText(player1.getTilesAsString());
        score1Label.setText("Player1 Tiles:");
        //score2Label.setText(player2.getTilesAsString());
        score2Label.setText("Player2 Tiles:");
        scorePanel.add(score1Label);
        scorePanel.add(score2Label);
    }

    public void updateScores() {
        String score1 = "Player1 Tiles: ";
        String score2 = "Player2 Tiles: ";
        score1 = score1 + ": " + " (" + player1.getScore() + ") points!\n" + "";
        score2 = score2 + ": " + " (" + player2.getScore() + ") points!\n" + "";
        score1Label.setText(score1);
        score2Label.setText(score2);
    }

    public void updateBoard() {
        for (int row = 0; row < board.getSize(); row++) {
            for (int column = 0; column < board.getSize(); column++) {
                Tile tile = board.getTile(row, column);
                tile.setContent(tiles[row][column].getText());
                if(tile.isAnchor()){
                    tiles[row][column].setBackground(Color.LIGHT_GRAY);
                }
            }
        }
        for (int i=0; i<toDisable.size(); i++){
            toDisable.get(i).removeActionListener(this);
        }
    }

    public void actionPerformed(ActionEvent e) {
        System.out.println("Button pressed!");
        if (e.getSource() == newGameButton) {
            System.out.println("Start new game needs to be implemented!");
            reset();
            return;
        }
        else if (e.getSource() == endTurnButton) {
            // Temp code until ai implemented
            if (currentPlayer == player2){
               this.currentPlayer = scrabbleGame.switchPlayer();
                return;
            }
            // end temp code
            String word = playedWord();
            System.out.println(word);
            if (word.length()<1 || !scrabbleGame.validMove(word)){
                System.out.println("Word not acceted!");
                return;
            }
            else{
                System.out.println("Word accepted!");
            }
            for (int i=0; i<tiles.length; i++){
                for (int j=0; j<tiles.length; j++){
                    if (board.getTile(i, j).containsLetter()){
                        String s = tiles[i][j].getText();
                        tiles[i][j].removeActionListener(this);
                        tiles[i][j].setText(s);
                    }
                }
            }

            this.currentPlayer = scrabbleGame.switchPlayer();
            played.clear();
            if(currentPlayer == player1)System.out.println("current player is: player1");
            else System.out.println("current player is: player2");

            return;

        }
        Point p = buttonInArray(e.getSource());
        if (p != null){
            System.out.println("Found button in array");
            gridButtonPressed((JButton)e.getSource(), p);
        }
    }

    public void gridButtonPressed(JButton button, Point p){
        if (currentPlayer != player1) return;
        String tilesAsString = new String();
        for (int i=0; i<playerTiles.size(); i++){
            tilesAsString += playerTiles.get(i);
            if (i < playerTiles.size()-1){
                tilesAsString += ", ";
            }

        }
        String s = (String) JOptionPane.showInputDialog(
                this,
                "Available letters:\n" + tilesAsString.toUpperCase(),
                "Place letter",
                JOptionPane.PLAIN_MESSAGE);

        if (s == null) return;
        if (s.length() > 1){
            return;
        }else if (s.length()<1){
            s=" ";
        }
        else if (playerTiles.contains((Character)s.charAt(0)) || s.equals(" ")){
            if (button.getText().equals(" ")){

            }
            else{
                playerTiles.add(button.getText().charAt(0));

            }
            button.setText(s);
            playerTiles.remove((Character)s.charAt(0));
            System.out.println("Adding point ("+p.getX()+", "+p.getY()+") to played");
            played.add(p);

        }
    }

    public Point buttonInArray(Object o){
        System.out.println("Checking if button is in array");
        for (int i=0; i<board.getSize(); i++){
            for (int j=0; j<board.getSize(); j++){
                if(tiles[i][j].equals(o))
                    return new Point(j,i);
            }
        }
        return null;
    }

    private boolean Point(int i, int i0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String playedWord(){
        Collections.sort(this.played, new PointCompare());
        String s = new String();


        if (played.get(0).getX() == played.get(played.size()-1).getX()){
            System.out.println("found that y is equal");

            int range = (int)played.get(played.size()-1).getY() - (int)played.get(0).getY();
            range += (int)played.get(0).getX();
            for (int i=(int)played.get(0).getX(); i < range+1; i++){
                s += tiles[i][(int)played.get(0).getY()].getText();
                System.out.println("s is now: "+s);
            }

        }
        else{
            System.out.println("found that x is equal");

            int range = (int)played.get(played.size()-1).getX() - (int)played.get(0).getX();
            System.out.println("Range is: "+range);
            range += (int)played.get(0).getY();
            for (int i=(int)played.get(0).getY(); i < range+1; i++){
                System.out.println("Coords of checked letter: ");
                s += tiles[(int)played.get(0).getX()][i].getText();
                System.out.println("s is now: "+s);
            }
        }
        return s;
    }

    public void wordFinder(Point startPoint, Point endPoint){
        int startX = (int)startPoint.getX();
        int startY = (int)startPoint.getY();
        int endX = (int)endPoint.getX();
        int endY = (int)endPoint.getY();
        if (startX == endX){
            for (int i=startY; i>=0; i--){
                if (!tiles[startX][i].equals(" ")){
                    played.add(new Point(startX, i));
                }
            }
            for (int i = endY; i<board.getSize(); i++){
                if (!tiles[startX][i].equals(" ")){
                    played.add(new Point(startX, i));
                }

            }
        }
        if(startY==endY){
            for (int i=startX; i>=0; i--){
                if (!tiles[i][startY].equals(" ")){
                    played.add(new Point(i, startY));
                }
            }
            for (int i = endY; i<board.getSize(); i++){
                if (!tiles[i][startY].equals(" ")){
                    played.add(new Point(i, startY));
                }

            }
        }
    }

    public class PointCompare
            implements Comparator<Point> {

        public int compare(final Point a, final Point b) {
            if (a.x < b.x) {
                return -1;
            }
            else if (a.x > b.x) {
                return 1;
            }
            else {
                return 0;
            }
        }
    }

    class GridAction extends AbstractAction {

        private Frame window;
        private Player currentPlayer;
        public GridAction(Frame frame, Player currentPlayer) {
            this.window = frame;
            this.currentPlayer = currentPlayer;
        }

        public void actionPerformed(ActionEvent e) {

        }
    }


    public void reset() {
        // Reset Game
    }

}
